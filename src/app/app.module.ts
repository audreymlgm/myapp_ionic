import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ProductsPage } from '../pages/products/products';
import { ProductdetailsPage } from '../pages/productdetails/productdetails';
import { ProspectsPage } from '../pages/prospects/prospects';7
import { ProspectdetailsPage } from '../pages/prospectdetails/prospectdetails';
import { DevisPage } from '../pages/devis/devis';
import { DevisdetailsPage } from '../pages/devisdetails/devisdetails';
import { LoginPage } from '../pages/login/login';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { EditproductPage } from '../pages/editproduct/editproduct';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BackendProvider } from '../providers/backend/backend';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ProductsPage,
    ProductdetailsPage,
    ProspectsPage,
    ProspectdetailsPage,
    DevisPage,
    DevisdetailsPage,
    LoginPage,
    SignUpPage,
    EditproductPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ProductsPage,
    ProductdetailsPage,
    ProspectsPage,
    ProspectdetailsPage,
    DevisPage,
    DevisdetailsPage,
    LoginPage,
    SignUpPage,
    EditproductPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BackendProvider,
    Geolocation,
  ]
})
export class AppModule {}
