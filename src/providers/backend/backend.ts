// import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';



// onGoDetails(id) {
//   this.navCtrl.setRoot(Prod
@Injectable()
export class BackendProvider {

  constructor(public http: Http) {
    console.log('Hello BackendProvider Provider');
  }

  allProductsFromLaravel(){
    return this.http.get('api/products').map(response => response.json())
    }

  ProductFromLaravel(id){
    return this.http.get('api/productsdetails/' + id).map(response => response.json())
  }

  allProspectsFromLaravel(){
    return this.http.get('api/prospects').map(response => response.json())
  }

  ProspectFromLaravel(id){
    return this.http.get('api/prospect/' + id).map(response => response.json())
  }

  // addProductFromLaravel() {
  //   return this.http.get('api/addproduct/').map(response => response.json() )
  // }

  newProductFromLaravel(products) {
    console.log('products ' + JSON.stringify(products))
    return this.http.post('api/addproducts/', products).map(response => response.json() )
  }

  deleteProductFromLaravel(id) {
    return this.http.delete('api/products/'+ id, id).map(response => response.json() )
  }

  updateProductFromLaravel(id, product) {
    console.log(product)
    return this.http.post('api/editproducts/'+ id, product).map(response => response.json() )
  }

  newProspectFromLaravel(prospect){
    return this.http.post('api/prospect/', prospect).map(response => response.json() )
  }

}
