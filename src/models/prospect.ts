import { Society } from '../models/society';

export interface Prospect {

    nom : string;
    prenom : string;
    society_id :  string;
    mail: string;
    tel: string;
    client: boolean;

  }

