
export interface Product {

    nom : string;
    prix: number;
    photo :  string;
    fabricant: string;
    delai: number;
    description: string;
    id?: number;
    latitude?: number;
    longitude?: number;


  }
