// import { Product } from './product';
// import { Prospect } from './prospect';

export interface Devis {

    product : string,
    prospect : string,
    quantity :  number;
    price: number;
    subtotal : number;
    total : number;
    society : string,
    status: boolean,
  
  }
  