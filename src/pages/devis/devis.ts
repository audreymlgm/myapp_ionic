import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DevisdetailsPage } from '../devisdetails/devisdetails';
import { Devis } from '../../models/devis';


@Component({
  selector: 'page-devis',
  templateUrl: 'devis.html',
})

export class DevisPage {

  public devis : Devis

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertController: AlertController) {
  }

  onLoadDevis(devis: {prospect:string, society: string, price:number, product:string, quantity: number, subtotal: number, total: number, status: boolean}) {
    this.navCtrl.push(DevisdetailsPage, {devis : devis});

  }

  openInfosAlert() {
    let addInfosAlert = this.alertController.create({
      title: "Ajoutez vos devis",
      message: "Ajoutez des devis",
      inputs: [
        {
        type:"text",
        name:"prospect",
        placeholder:"Nom du Prospect",
        },

        {
        type:"text",
        name:"society",
        placeholder:"Société du Prospect",
      },

        {
          type:"text",
          name:"product",
          placeholder:"Nom du Produit",
        },
        {
          type:"number",
          name:"price",
          placeholder:"Prix du Produit",
        },
        {
          type:"number",
          name:"quantity",
          placeholder:"Quantité du Produit",
        },
        {
          type:"number",
          name:"subtotal",
          placeholder:"Sous-total",
        },
        
        {
          type:"boolean",
          name:"status",
          placeholder:"Statut du Devis",
        },
        
      ],

      buttons : [
        {
          text: "Annuler"
        },
        {
          text: "Valider",
          handler: (inputData) =>{
            
            this.devis = {
              prospect : inputData.prospect,
              product : inputData.product,
              society : inputData.society,
              price : inputData.price,
              quantity : inputData.quantity,
              subtotal : inputData.subtotal,
              total : Number(inputData.price) * Number(inputData.quantity),
              status : inputData.status,
            }
            
              this.navCtrl.push(DevisdetailsPage, {
                devis : this.devis
              })
            }
          }]


    })

    addInfosAlert.present();
  }


  devisList = [
    {
      prospect : 'Dupont Francois',
      society : 'Auchan',
      product: 'Canard en Plastique',
      price : '2',
      quantity: '4',
      subtotal: '8',
      status : 'En cours',
      total : '8',
      

    },
    {
      prospect : 'Robinson Samuel',
      society : 'Carrefour',
      product: 'Glaces',
      price: '4',
      quantity: '4',
      subtotal: '16',
      status: 'Valid',
      total: '16',
    

   
    },
    {
      prospect : 'Nelson Manuel',
      society : 'Carrefour',
      product : 'Bouees de Sauvetage',
      price : '15',
      quantity : '10',
      subtotal : '150',
      status : 'Valid',
      total: '150',
 

      
    }
  ];

  
}
