import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
// import { BackendProvider } from '../../providers/backend/backend';
import { Product } from '../../models/product';




@Component({
  selector: 'page-productdetails',
  templateUrl: 'productdetails.html',
})

export class ProductdetailsPage {

  public product: Product


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.product  = this.navParams.get('product')
    console.log(this.product)
  }

  // ionViewDidLoad() {
  //   this.BackendProvider.ProductFromLaravel(this.id).subscribe(products => { this.product = products
  //     console.log(this.product)
  //   });
  // }

}
