import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ProductdetailsPage } from '../productdetails/productdetails';
import { Product } from '../../models/product';
import { BackendProvider } from '../../providers/backend/backend';
import { EditproductPage } from '../../pages/editproduct/editproduct';


@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})

export class ProductsPage {
  // p: undefined;
  public products : Product[];
  public product : Product;
  public id = ''


  constructor(public navCtrl: NavController, public navParams: NavParams, public alertController: AlertController, public BackendProvider: BackendProvider, public ToastController : ToastController) {
    this.product  = this.navParams.get('product');
  }


  onGoToEdit(id) {
    this.BackendProvider.ProductFromLaravel(id).subscribe(product => {
    this.product = product
    this.navCtrl.push(EditproductPage, {
        product:this.product

      })
    });

  }

  onGoToDelete(id) {
  this.BackendProvider.deleteProductFromLaravel(id).subscribe(response => {
    // console.log('test1')
    this.navCtrl.setRoot(ProductsPage)
  },error => {
    // console.log('test')
  })
    const toast = this.ToastController.create({
      message: 'Votre produit a bien été supprimé',
      duration: 2000
    });
    toast.present();

  }


  ionViewDidLoad() {
    this.BackendProvider.allProductsFromLaravel().subscribe(products => {
      // console.log('products ' + JSON.stringify(products))
      this.products = products

    })
  }

    onGoToDetails(id) {

      this.BackendProvider.ProductFromLaravel(id).subscribe(product => { this.product = product
      this.navCtrl.push(ProductdetailsPage, {
       product: this.product

      }
      )}

      )};



  // onLoadProduct(product: {name:string, description : string[]}) {
  //   this.navCtrl.push(ProductdetailsPage, {product : product});
  // }

  openInfosAlert() {
    let addInfosAlert = this.alertController.create({
      title: "Ajoutez vos produits",
      message: "Ajoutez des produits",
      inputs: [
        {
        type:"text",
        name:"name",
        placeholder:"Nom du Produit",
        },

        {
        type:"text",
        name:"price",
        placeholder:"Prix du Produit",
      },

        {
          type:"text",
          name:"mark",
          placeholder:"Fabricant du Produit",
        },
        {
          type:"text",
          name:"description",
          placeholder:"Description du Produit",
        },
        {
          type:"text",
          name:"photo",
          placeholder:"URL du Produit",
        },
        {
          type:"text",
          name:"delai",
          placeholder:"Delai de livraison du Produit",
        },

      ],


      buttons : [
        {
          text: "Annuler"
        },
        {
          text: "Valider",
          handler: (inputData) =>{

            this.product = {
              nom : inputData.name,
              prix : inputData.price,
              fabricant : inputData.mark,
              description: inputData.description,
              photo : inputData.photo,
              delai : inputData.delai,

            }

            console.log(inputData.description);
            this.BackendProvider.newProductFromLaravel(this.product).subscribe(data => {
              console.log('youhou')
            },error => {
              console.log('test')


              // console.log('product ' + JSON.stringify(this.product))
              // this.products = products

            })


            }
          }]


    })

    addInfosAlert.present();
  }


  // productsList = [
  //   {
  //     name : 'Glaces',
  //     description: [
  //       'Prix : 4 euros',
  //       'Marque : Fait maison'
  //     ]
  //   },


  //     name: 'Canards en Plastique',
  //     description : [
  //       'Prix : 2 euros',
  //       'Marque : Amazon'
  //     ]
  //   },
  //   {
  //     name: 'Bouées gonflables',
  //     description : [
  //       'Prix : 15 euros',
  //       'Marque : Cdiscount'
  //     ]
  //   }
  // ];



}


