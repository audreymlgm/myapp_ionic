import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BackendProvider } from '../../providers/backend/backend';
import { Prospect } from '../../models/prospect';
import { Society } from '../../models/society';


@Component({
  selector: 'page-prospectdetails',
  templateUrl: 'prospectdetails.html',
})
export class ProspectdetailsPage {

  public prospect: Prospect
  public society : Society

  constructor(public navCtrl: NavController, public navParams: NavParams, public BackendProvider : BackendProvider) {
    this.prospect  = this.navParams.get('prospect');
       }

}
