import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-devisdetails',
  templateUrl: 'devisdetails.html',
})
export class DevisdetailsPage {

  public devis: {
    prospect: string,
    society: string,
    validated: string

  }
 

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.devis  = this.navParams.get('devis');
   }
  
  
  

}
