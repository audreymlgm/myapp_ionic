import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { ProspectdetailsPage } from '../prospectdetails/prospectdetails';
import { BackendProvider } from '../../providers/backend/backend';
import { Prospect } from '../../models/prospect';

@Component({
  selector: 'page-prospects',
  templateUrl: 'prospects.html',
})

export class ProspectsPage {

  public prospects : Prospect[];
  public prospect : Prospect;
  public id = ''

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertController: AlertController, public BackendProvider : BackendProvider, public ToastController : ToastController) {
  }

  ionViewDidLoad() {
    this.BackendProvider.allProspectsFromLaravel().subscribe(prospects => {
      console.log('prospects ' + JSON.stringify(prospects))
      this.prospects = prospects
    });
  }

  onGoToDetailsProspect(id) {
    this.BackendProvider.ProspectFromLaravel(id).subscribe(prospect => { this.prospect = prospect
      this.navCtrl.push(ProspectdetailsPage, {
        prospect: this.prospect
      }
      )}

      )};

  onGoToAddProspect(prospect) {
      this.BackendProvider.newProspectFromLaravel(prospect).subscribe(prospect => { this.prospect = prospect
        // console.log('test1')
        this.navCtrl.setRoot(ProspectsPage)
      },error => {
        // console.log('test')
      })
        const toast = this.ToastController.create({
          message: 'Votre prospect a bien été ajouté',
          duration: 2000
        });
        toast.present();

      }
  }

  // onLoadProspect(prospect: {name:string, firstname : string, society:string, customer:string, mail:string, phone:string}) {
  //   this.navCtrl.push(ProspectdetailsPage, {prospect : prospect});
  // }

  // openInfosAlert() {
  //   let addInfosAlert = this.alertController.create({
  //     title: "Ajoutez vos prospects",
  //     message: "Ajoutez des prospects",
  //     inputs: [
  //       {
  //       type:"text",
  //       name:"name",
  //       placeholder:"Nom du Prospect",
  //       },

  //       {
  //       type:"text",
  //       name:"firstname",
  //       placeholder:"Prénom du prospect",
  //     },

  //       {
  //         type:"text",media/tCt4919VvQKCA/giphy.gif
  //         name:"society",
  //         placeholder:"Société du prospect",
  //       },
  //       {
  //         type:"text",
  //         name:"phone",
  //         placeholder:"Téléphone du prospect",
  //       },
  //       {
  //         type:"text",
  //         name:"mail",
  //         placeholder:"Email du prospect",
  //       },
  //       {
  //         type:"text",
  //         name:"customer",
  //         placeholder:"Le prospect est-il client ?",
  //       }
  //     ],

  //     buttons : [
  //       {
  //         text: "Annuler"
  //       },
  //       {
  //         text: "Valider",
  //         handler: (inputData) =>{

  //           this.prospect = {
  //             name : inputData.name,
  //             firstname : inputData.firstname,
  //             society_id : inputData.society,
  //             phone : inputData.phone,
  //             mail : inputData.mail,
  //             customer : inputData.customer,
  //           }

  //             this.navCtrl.push(ProspectdetailsPage, {
  //               prospect : this.prospect
  //             })
  //           }
  //         }]


  //   })

  //   addInfosAlert.present();
  // }

  // prospectsList = [
  //   {
  //     name : 'Dupont',
  //     firstname : 'Francois',
  //     society : 'Auchan',
  //     customer: 'Oui',
  //     mail : 'dupontfrancois@hotmail.fr',
  //     phone : '0611111111'

  //   },
  //   {
  //     name : 'Robinson',
  //     firstname : 'Samuel',
  //     society : 'Carrefour',
  //     customer: 'Non',
  //     mail : 'robinsonsamuel@hotmail.fr',
  //     phone : '06222222'
  //   },
  //   {
  //     name : 'Nelson',
  //     firstname : 'Manuel',
  //     society : 'Carrefour',
  //     customer: 'Oui',
  //     mail : 'nelsonmanuel@hotmail.fr',
  //     phone : '06333333'
  //   }
  // ];



