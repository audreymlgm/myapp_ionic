import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProductsPage } from '../products/products';
import { ProspectsPage } from '../prospects/prospects';
import { DevisPage } from '../devis/devis';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  onGoToProducts() {
    this.navCtrl.push(ProductsPage);
  }

  onGoToProspects() {
    this.navCtrl.push(ProspectsPage);
  }

  onGoToDevis() {
    this.navCtrl.push(DevisPage);
  }

}
