import { Component, ViewChild, ElementRef } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { Product } from '../../models/product';
import { BackendProvider } from '../../providers/backend/backend';
import { ProductsPage } from '../../pages/products/products';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import leaflet from 'leaflet';


@Component({
  selector: 'page-editproduct',
  templateUrl: 'editproduct.html',

})

export class EditproductPage {

  // @ViewChild('map') mapContainer: ElementRef;

 map : any;
 public product: Product;
 validateForm : FormGroup


  constructor(public navCtrl: NavController, public navParams: NavParams, public BackEndProvider: BackendProvider, public FormBuilder: FormBuilder, public Geolocation : Geolocation) {
    this.product  = this.navParams.get('product')
    // console.log(this.product)


    this.validateForm = FormBuilder.group({
    nom  : ['', [Validators.required, Validators.minLength(4)]],
    prix : ['', Validators.compose([ Validators.required, Validators.pattern('[0-9]*')])],
    photo : ['', Validators.required],
    fabricant : ['', [Validators.required, Validators.minLength(4)]],
    delai : ['', null],
    description : ['', Validators.required],
  })
}



  onGoToUpdate(id) {
    if (this.validateForm.valid) {
    this.BackEndProvider.updateProductFromLaravel(id, this.product).subscribe(response => {
      this.navCtrl.setRoot(ProductsPage)
      console.log('youhou')
  }, error => {
    console.log('test')
  })

  }
}

ionViewDidLoad() {
  this.locate();
  this.loadmap();
}

loadmap() {
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18
    }).addTo(this.map);
    this.map.locate({
      setView: true,
      maxZoom: 10
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([e.latitude, e.longitude]).on('click', () => {
        alert('Marker clicked');
      })
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
      }).on('locationerror', (err) => {
        alert(err.message);
    })

  }


locate() {
  this.Geolocation.getCurrentPosition().then((resp) => {
    // resp.coords.latitude
    // resp.coords.longitude
    console.log(resp.coords)
    this.product.latitude = resp.coords.latitude
    this.product.longitude = resp.coords.longitude
    // this.data =  'Lat: ' + resp.coords.latitude  + 'Lng: ' + resp.coords.longitude
   }).catch((error) => {
     console.log('Error getting location', error);
   });
}

}

